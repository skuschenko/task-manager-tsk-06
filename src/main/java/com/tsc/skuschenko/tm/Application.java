package com.tsc.skuschenko.tm;

import com.tsc.skuschenko.tm.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        System.out.println("***Welcome to task manager***");
        if (parseArgs(args)) exit();
        final Scanner scanner = new Scanner(System.in);
        while (true){
            System.out.print("Enter command:");
            final String command  = scanner.nextLine();
            parseArg(command);
        }
    }

    private static void parseArg(String arg) {
        if (arg == null ) return;
        switch (arg.toLowerCase()) {
            case TerminalConst.TM_ABOUT:   showAbout();   break;
            case TerminalConst.TM_VERSION: showVersion(); break;
            case TerminalConst.TM_HELP:    showHelp();    break;
            case TerminalConst.TM_EXIT:    exit();        break;
            default:                       showWrong(arg);break;
        }
    }

    private static void showWrong(String arg) {
        System.out.print(arg + " command doesn't  found. ");
        System.out.println("Pleas print '" + TerminalConst.TM_HELP + "' for more information");
    }

    private static boolean parseArgs(String[] args) {
        if (args == null || args.length<1 ) return false;
        final String arg= args[0];
        parseArg(arg);
        return true;
    }

    private static void showAbout() {
        System.out.println("["+ TerminalConst.TM_ABOUT.toUpperCase() +"]");
        System.out.println("AUTHOR: Semyon Kuschenko");
        System.out.println("EMAIL: skushchenko@tsconsulting.com");
    }

    private static void showVersion() {
        System.out.println("["+ TerminalConst.TM_VERSION.toUpperCase() +"]");
        System.out.println("1.0.0");
    }
    
    private static void showHelp() {
        System.out.println("["+ TerminalConst.TM_HELP.toUpperCase() +"]");
        System.out.println(TerminalConst.TM_HELP+" - show help information");
        System.out.println(TerminalConst.TM_VERSION+" - show version application");
        System.out.println(TerminalConst.TM_ABOUT+" - show about information");
        System.out.println(TerminalConst.TM_EXIT+" - close program");
    }

    private static void exit() {
        System.exit(0);
    }

}
