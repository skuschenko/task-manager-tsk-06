package com.tsc.skuschenko.tm.constant;

public class TerminalConst {
    public static final String TM_VERSION ="version";
    public static final String TM_HELP ="help";
    public static final String TM_ABOUT ="about";
    public static final String TM_EXIT ="exit";
}
